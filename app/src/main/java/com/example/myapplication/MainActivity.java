package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.DragEvent;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tv1 = findViewById(R.id.weather1);
        et1 = findViewById(R.id.editText);
        weatherMin = findViewById(R.id.weathermin);
        weatherMax = findViewById(R.id.weathermax);
        currentCity = findViewById(R.id.city);
        feeling = findViewById(R.id.feeling);
        switch1 = findViewById(R.id.switch1);
        latText = findViewById(R.id.LatText);
        lngText = findViewById(R.id.LngText);

        switch1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    latText.setVisibility(View.VISIBLE);
                    lngText.setVisibility(View.VISIBLE);
                    et1.setVisibility(View.INVISIBLE);
                }else{
                    latText.setVisibility(View.INVISIBLE);
                    lngText.setVisibility(View.INVISIBLE);
                    et1.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    TextView tv1;
    Switch switch1;
    TextView weatherMin, weatherMax, currentCity, feeling;
    EditText et1, latText, lngText;

    public void click(View view) {
        if (!switch1.isChecked()){
            String url = "https://api.openweathermap.org/data/2.5/weather?q=" + et1.getText().toString() + "&appid=b1b35bba8b434a28a0be2a3e1071ae5b&units=metric&lang=ru";
            JsonObjectRequest jo = new JsonObjectRequest(Request.Method.GET, url, null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject main = response.getJSONObject("main");
                                String temp = String.valueOf(main.getDouble("temp"));
                                String h = String.valueOf(main.getDouble("temp_max"));
                                String l = String.valueOf(main.getDouble("temp_min"));
                                String city = response.getString("name");
                                String feelslike = String.valueOf(main.getDouble("feels_like"));
                                weatherMax.setText("Макс.: " + h);
                                weatherMin.setText("Мин.: " + l);
                                currentCity.setText(city);
                                tv1.setText(temp);
                                feeling.setText("Ощущается: " + feelslike);
                                weatherMax.setVisibility(View.VISIBLE);
                                weatherMin.setVisibility(View.VISIBLE);
                                tv1.setVisibility(View.VISIBLE);
                                currentCity.setVisibility(View.VISIBLE);
                                feeling.setVisibility(View.VISIBLE);
                            } catch (JSONException e) {
                                tv1.setText(e.getMessage());
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            tv1.setText(error.getMessage());
                        }
                    }
            );
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(jo);
        }else{
            String url = "https://api.openweathermap.org/data/2.5/weather?lat=" + latText.getText().toString() + "&lon=" + lngText.getText().toString() + "&appid=b1b35bba8b434a28a0be2a3e1071ae5b&units=metric&lang=ru";
            JsonObjectRequest jo = new JsonObjectRequest(Request.Method.GET, url, null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject main = response.getJSONObject("main");
                                String temp = String.valueOf(main.getDouble("temp"));
                                String h = String.valueOf(main.getDouble("temp_max"));
                                String l = String.valueOf(main.getDouble("temp_min"));
                                String city = response.getString("name");
                                String feelslike = String.valueOf(main.getDouble("feels_like"));
                                weatherMax.setText("Макс.: " + h);
                                weatherMin.setText("Мин.: " + l);
                                currentCity.setText(city);
                                tv1.setText(temp);
                                feeling.setText("Ощущается: " + feelslike);
                                weatherMax.setVisibility(View.VISIBLE);
                                weatherMin.setVisibility(View.VISIBLE);
                                tv1.setVisibility(View.VISIBLE);
                                currentCity.setVisibility(View.VISIBLE);
                                feeling.setVisibility(View.VISIBLE);
                            } catch (JSONException e) {
                                tv1.setText(e.getMessage());
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            tv1.setText(error.getMessage());
                        }
                    }
            );
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(jo);
        }

    }

    public void switchMode(View view){
        if(switch1.isChecked()){
            latText.setVisibility(View.VISIBLE);
            lngText.setVisibility(View.VISIBLE);
            et1.setVisibility(View.INVISIBLE);
        }else{
            latText.setVisibility(View.INVISIBLE);
            lngText.setVisibility(View.INVISIBLE);
            et1.setVisibility(View.VISIBLE);
        }
    }
}
